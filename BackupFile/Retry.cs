﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace BackupFile
{
	public static class Retry
    {
        public static void Loop(
            Action action,
            TimeSpan retryInterval,
            int maxAttemptCount = 3)
        {
            Loop<object>(() =>
            {
                action();
                return null;
            }, retryInterval, maxAttemptCount);
        }

        public static T Loop<T>(
            Func<T> action,
            TimeSpan retryInterval,
            int maxAttemptCount = 3)
        {
            var exceptions = new List<Exception>();

            for (int attempted = 0; attempted < maxAttemptCount; attempted++)
            {
                try
                {
                    if (attempted > 0)
                    {
                        Thread.Sleep(retryInterval);
                    }
                    return action();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    exceptions.Add(ex);
                }
            }

            throw new AggregateException(exceptions);
        }
    }
}
