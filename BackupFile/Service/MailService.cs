﻿using log4net;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BackupFile.Service
{
	public class MailService
	{
		public void SendMail(IEnumerable<string> mailMessages, IEnumerable<string> recipients,ILog logger)
		{
            using (SmtpClient client = new SmtpClient())
			{
                logger.Info("Start sending mail");
                using (MailMessage message = new MailMessage())
                {
                    foreach(var item in recipients)
					{
                        message.To.Add(item);
                    }
                    message.IsBodyHtml = false;
                    message.Subject = $"Backup database fail {DateTime.Now.ToString("dd/MM/yyyy")}";
                    message.Body = $"Backup fail: ";
                    foreach(var s in mailMessages)
					{
                        message.Body = message.Body + $"{Environment.NewLine}{s}";
                    }
                    try
                    {
                        client.Send(message);
                        logger.Info("Finish sending mail");
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"Send mail fail: {ex.Message}");
                    }
                }
            }

        }
	}
}
