﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Logging;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace BackupFile
{
    public class GoogleDriveService
    {
        private readonly DriveService _driveService;
        private readonly ILog _logger;
        public GoogleDriveService(UserCredential credential, string applicationName, ILog logger)
        {
            _driveService = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = applicationName,
            });
            _logger = logger;
        }

        public UserCredential Authenticate(string credPath, string[] scopes)
        {
            using (var stream = new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                return GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }
        }

        public IList<Google.Apis.Drive.v3.Data.File> GetFiles(string folderId = "root", DateTime? endDate = null, DateTime? startDate = null)
        {
            FilesResource.ListRequest request = _driveService.Files.List();
            request.PageSize = 1000;
            request.Fields = "nextPageToken, files(id, name, parents, createdTime, modifiedTime, mimeType)";
            request.Q = $"'{folderId}' in parents  and trashed = false";
			if (endDate.HasValue)
			{
                request.Q += $" and modifiedTime < '{endDate.Value:yyyy-MM-ddThh:mm:ss}'";
            }
            if (startDate.HasValue)
            {
                request.Q += $" and modifiedTime >= '{startDate.Value:yyyy-MM-ddThh:mm:ss}'";
            }
            return request.Execute().Files;
        }

        public string DeleteFile(string fileId)
        {
            try
            {
                var request = _driveService.Files.Delete(fileId);
                return request.Execute();
            }
            catch (Exception ex)
            {
                var message = $"Delete file {fileId} fail: {ex.Message}";
                throw new Exception(message);
            }
        }

        public Google.Apis.Drive.v3.Data.File CreateFolder(string name, string folderId = "root")
        {

            try
            {
                var fileMetadata = new Google.Apis.Drive.v3.Data.File()
                {
                    Name = name,
                    MimeType = "application/vnd.google-apps.folder",
                    Parents = new[] { folderId }
                };

                var request = _driveService.Files.Create(fileMetadata);
                request.Fields = "id, name, parents, createdTime, modifiedTime, mimeType";

                return request.Execute();
            }
            catch (Exception ex)
            {
                var message = $"Create file fail: {ex.Message}";
                //_logger.Error(message);
                throw new Exception(message);
            }
        }

        public Google.Apis.Drive.v3.Data.File Upload(FileInfo file, string folderId = "root")
        {
            if(file == null)
			{
                throw new Exception("File to upload is empty");
			}
            var fileUpload = new Google.Apis.Drive.v3.Data.File
            {
                Name = DateTime.Now.ToString("dd/MM/yyyy-").ToString() + file.Name,
                Description = "Backup file test",
                MimeType = MimeMapping.GetMimeMapping(file.Name),
                Parents = new[] { folderId }
            };

            FilesResource.CreateMediaUpload request;

            using (var stream = new FileStream(file.FullName, FileMode.Open))
            {
                _logger.Info("Start uploading");
                request = _driveService.Files.Create(fileUpload, stream, fileUpload.MimeType);
                request.Upload();
				if (request.ResponseBody == null)
				{
                    var message = $"Upload file fail";
                    //Console.WriteLine("Upload fail");
                    throw new Exception(message);
				}
                _logger.Info("Finish uploading");
            }

            return request.ResponseBody;
        }

    }
}
