﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackupFile
{
	public class FileService
	{
		private readonly ILog _logger;
		public FileService(ILog logger)
		{
			_logger = logger;
		}

		public FileInfo GetLastestFile(string folderPath)
		{
			try
			{
				var dir = new DirectoryInfo(folderPath);
				return dir.GetFiles(".").OrderBy(s => s.CreationTime).LastOrDefault();
			}
			catch(Exception ex)
			{
				_logger.Error($"Get file local fail: {ex.Message}");
			}
			return null;

		}
	}
}
