﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Logging;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackupFile.Service
{
	public class BackupService
	{
        static string ApplicationName = "Drive API .NET Quickstart";

        private readonly ILog _logger;
        public BackupService(ILog logger)
		{
            _logger = logger;
        }

        public void Backup(string filePath, string driveFolderId, UserCredential credential, int dayToDelete)
        {

            var driveService = new GoogleDriveService(credential, ApplicationName, _logger);
            var fileService = new FileService(_logger);
            //upload latest file to backup
            var lastestfile = fileService.GetLastestFile(filePath);
            driveService.Upload(lastestfile, driveFolderId);

            //delete old file on drive
            var deleteFiles = driveService.GetFiles(driveFolderId, DateTime.UtcNow.AddDays(-dayToDelete));
            if(deleteFiles.Count() == 0)
			{
                _logger.Info("No file to delete");
                return;
			}

            _logger.Info("start delete old file");
            deleteFiles.ToList().ForEach(s => driveService.DeleteFile(s.Id));
            _logger.Info("finish delete old file");
        }
    }
}
