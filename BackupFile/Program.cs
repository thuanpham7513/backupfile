﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Logging;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BackupFile.Service;
using log4net;
using System.Configuration;

namespace BackupFile
{
    class Program
    {
        static string[] Scopes = { DriveService.Scope.Drive };
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            if (args.Count() != 3)
            {
                return;
            }
            var folderId = ConfigurationManager.AppSettings["folderId"];
            var localFolderPath = ConfigurationManager.AppSettings["localFolder"];
            var retryTime = Int32.Parse(ConfigurationManager.AppSettings["retryTimes"]);
            var dayToDelete = Int32.Parse(ConfigurationManager.AppSettings["dayToDelete"]);
            try
			{
                var credential = AuthorizeHelper.Authenticate("credentials.json", Scopes);
                logger.Info("Start backup");
                var backupservice = new BackupService(logger);
                Retry.Loop(() => backupservice.Backup(localFolderPath, folderId, credential, dayToDelete), 
                           TimeSpan.FromSeconds(1),
                           retryTime);
                logger.Info("Finish backup");
            }
            catch(AggregateException ex)
			{
                logger.Error(ex.InnerExceptions.Select(s => s.Message));
                //fail all then send mail
                if(ex.InnerExceptions.Count() == retryTime)
				{
                    var recipients = ConfigurationManager.AppSettings["recipients"];
                    var listRecipients = new List<string>();
                    foreach (var address in recipients.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        listRecipients.Add(address);
                    }
                    var mailService = new MailService();
                    mailService.SendMail(ex.InnerExceptions.Select(s => s.Message), listRecipients, logger);
                }

            }
            
            Console.ReadKey();
        }
   
    }
}
